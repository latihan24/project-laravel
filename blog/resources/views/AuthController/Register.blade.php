@extends('layout.master')

@section('judul')
Halaman Form
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label >Gender</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label >Nationality</label><br><br>
        <select name="Nationality" id="">
            <option value="a">Indonesia</option>
            <option value="a">Amerika</option>
            <option value="a">inggris</option>
        </select><br><br>
        <label >Language Spoken</label><br><br>
        <input type="checkbox" name="Language Spoken">Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken">English <br>
        <input type="checkbox" name="Language Spoken">Other<br><br>
        <label >Bio</label><br><br>
        <textarea name="bio"  cols="30" rows="10"></textarea><br><br>
        <input type="submit"value="Sign Up">
    </form>
    @endsection
