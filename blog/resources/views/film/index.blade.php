
@extends('layout.master')

@section('judul')
List Film
@endsection
@section('content')
@auth
  <a href="/film/{{$genre->id}}/create" class="btn btn-secondary mb-3">Tambah Kategori</a>
@endauth


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">judul</th>
        <th scope="col">Ringkasan</th>
        <th scope="col">Tahun</th>
        <th scope="col">Poster</th>
        
      </tr>
    </thead>
    <tbody>
        @forelse ($films as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->judul}}</td>
          <td>{{$item->ringkasan}}</td>
          <td>{{$item->tahun}}</td>
          <td>{{$item->poster}}</td>
            
          <td>
                @auth
                <form action="/film/{{$genre->id}}/{{$item->id}}" method="POST">
                  <a href="/film/{{$genre->id}}/{{$item->id}}" class="btn btn-info btn-sm">Show</a>
            
                  <a href="/film/{{$genre->id}}/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form> 
                @endauth

                @guest
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Show</a>
                @endguest
              </td>
              </tr>
            
        @empty
        <h1>No Data</h1>
            
        @endforelse
      
    </tbody>
  </table>
    
    @endsection
    