@extends('layout.master')

@section('judul')

Edit Film
@endsection
@section('content')

<form action="/film/{{$film -> id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >film</label>
      <input type="text" name="nama" value="{{$film->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="text" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea name="bio" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

    

    @endsection