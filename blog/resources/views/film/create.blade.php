@extends('layout.master')

@section('judul')
Tambah Film
@endsection
@section('content')

<form action="film/{{$genre->id}}" method="POST">
    @csrf
    <div class="form-group">
      <label >Judul </label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Ringkasan</label>
      <input type="text" name="ringkasan" class="form-control">
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Tahun</label>
      <textarea name="tahun" class="form-control"></textarea>
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Poster</label>
      <textarea name="poster" class="form-control"></textarea>
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

    
    @endsection
    

