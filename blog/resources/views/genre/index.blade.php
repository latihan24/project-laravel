@extends('layout.master')

@section('judul')
List Genre
@endsection
@section('content')
@auth
  <a href="/genre/create" class="btn btn-secondary mb-3">Tambah Kategori</a>
@endauth


<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Jenis Genre</th>
        
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            
          <td>
                @auth
                <form action="/genre/{{$item->id}}" method="POST">
                  <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Show</a>
            
                  <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form> 
            <a href="/film/{{$item->id}}" class="btn btn-warning btn-sm">Film</a>
                @endauth

                @guest
                <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Show</a>
                @endguest
              </td>
              </tr>
            
        @empty
        <h1>No Data</h1>
            
        @endforelse
      
    </tbody>
  </table>
    
    @endsection
    