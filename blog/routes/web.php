<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('/register', 'FormController@bio');

Route::post('/welcome', 'FormController@kirim');

Route::get('/data-table', function (){
    return view ('table.data-table');
});


Route::group([
    'prefix'=> 'genre',
    'name'=>'genre.'
], function (){
     //CRUD Genre
Route::get('/create','GenreController@create')->name('create'); //route menuju form create
Route::post('/','GenreController@store')->name('store'); // untuk menyimpan data ke database
//read
Route::get('/','GenreController@index')->name('index'); //route list genre
Route::get('/{genre}','GenreController@show')->name('show'); //route detail genre
//update
Route::get('/{genre}/edit','GenreController@edit')->name('edit'); //route menuju ke form edit
Route::put('/{genre}','GenreController@update')->name('update'); //route untuk update data barang
//delete
Route::delete('/{genre}','GenreController@destroy')->name('destroy'); //route untuk hapus

});

Route::group([
    'prefix'=> 'film',
    'name'=>'film.'
], function (){
     //CRUD film
Route::get('/{genre}/create','FilmController@create')->name('create'); //route menuju form create
Route::post('/{genre}','FilmController@store')->name('store'); // untuk menyimpan data ke database
//read
Route::get('/{genre}','FilmController@index')->name('index'); //route list film
Route::get('/{genre}/{film}','FilmController@show')->name('show'); //route detail film
//update
Route::get('/{genre}/{film}/edit','FilmController@edit')->name('edit'); //route menuju ke form edit
Route::put('/{genre}/{film}','FilmController@update')->name('update'); //route untuk update data barang
//delete
Route::delete('/{genre}/{film}','FilmController@destroy')->name('destroy'); //route untuk hapus

});
   










//CRUD Cast
Route::get('/cast/create','CastController@create'); //route menuju form create
Route::post('/cast','CastController@store'); // untuk menyimpan data ke database
//read
Route::get('/cast','CastController@index'); //route list cast
Route::get('/cast/{cast_id}','CastController@show'); //route detail cast
//update
Route::get('/cast/{cast_id}/edit','CastController@edit'); //route menuju ke form edit
Route::put('/cast/{cast_id}','CastController@update'); //route untuk update data barang
//delete
Route::delete('/cast/{cast_id}','CastController@destroy'); //route untuk hapus






Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
