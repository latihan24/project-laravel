<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function create ()
    {
        return view ('film.create');
    }
    public function store ( Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => $Genre->id

        ]);
        $film = new Film;
 
        $film->judul = $request->nama;
        $film->ringkasan = $request->umur;
        $film->tahun = $request->bio;
        $film->poster = $request->poster;
 
        $film->save();

        return redirect('/film');
    }

    public function index ()
    {
        $film =  $genre->films
        return view ('film.index' , compact ('film'));
    }

    public function show ($film_id)
    {
        $film = Film::where('id', $film_id)->first();
        return view ('film.show' , compact ('film'));
    }

    public function edit ($film_id)
    {
        $film = Film::where('id', $film_id)->first();
        return view ('film.edit' , compact ('film'));
    }

    public function update (Request $request, $film_id)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $film = Film::find($film_id);
 
        $film->nama = $request ['nama'];
        $film->umur = $request ['umur'];
        $film->bio = $request ['bio'];
 
        $film->save();
        return redirect('/film');
    }
    public function destroy ($film_id)
    {
        $film = Film::find($film_id);
 
        $film->delete();
        return redirect('/film');
    }
}
