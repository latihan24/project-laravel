<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;

use Illuminate\Http\Request;


class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
 
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Genre $genre)
    {
        $film =  $genre ->films;
        return view ('film.index' , compact ('films','genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Genre $genre)
    {
        return view ('film.create', compact ('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , Genre $genre)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required'
        ]);
        Film::create([
            'judul' => $request->judul,
            'ringkasan' => $request->judul,
            'tahun' => $request->judul
        ]);

        return redirect()->route('film.index', compact('genre'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genre , Film $film)
    {
        
        return view ('genre.show' , compact ('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre , Film $film)
    {
        
        return view ('genre.edit' , compact ('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Genre $genre , Film $film)
    {
        $request->validate([
            'nama' => 'required|unique:genre',
            
        ]);

        
        $genre->nama = $request->nama;
        $genre->update();
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre, Film $film)
    {
        
        $genre->delete();
        return redirect('/genre');
    }
}
