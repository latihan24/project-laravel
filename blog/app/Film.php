<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'film';
    protected $guarded = [];

    public function kritiks ()
    {
        return $this->hasMany(kritik::class);
    }
    
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }
    //
    //protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster','genre_id'];
}
