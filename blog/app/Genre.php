<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';
    protected $guarded = [];
    public function films()
    {
        return $this->hasMany(Film::class);
    }
    //protected $table = 'genre';
    //protected $fillable = ['nama'];

}
