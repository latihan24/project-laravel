<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = 'kritik';
    protected $guarded = [];
    public function user()
    {
        return $this->belongTo(User::class);
    }

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

}
